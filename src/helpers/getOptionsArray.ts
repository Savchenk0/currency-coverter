import { optionT } from "../App"

type DataUnitType = {
	[key: string]: number | string | boolean
}

function getOptionsArray(data: DataUnitType[], property: string) {
	return data.reduce((acc, el) => {
		return [
			...acc,
			{ label: String(el[property]), value: String(el[property]) },
		]
	}, [] as optionT[])
}

export default getOptionsArray
