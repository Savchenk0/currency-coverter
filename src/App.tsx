import { Button, Flex, InputNumber, Select, Space, Typography } from "antd"
import { useEffect, useState } from "react"
import "./index.css"
import api from "./api/api"
import getOptionsArray from "./helpers/getOptionsArray"
import useSWR from "swr"
export type optionT = { label: string; value: string }
type currencyDataT = {
	code: string
	decimal_mark: string
	id: number
	name: string
	precision: string
	short_code: string
	subunit: number
	symbol: string
	symbol_first: boolean
	thousands_separator: string
}
const { Text } = Typography
function App() {
	const [inputValue, setInputValue] = useState<number | string | null>("")
	// const [testInputvalue, setTestInputvalue] = useState("")  remove when done
	// const [currencyList, setCurrencyList] = useState<optionT[]>([])
	const { data: currencyData } = useSWR<currencyDataT[]>(
		"currencyList",
		api.fetchCurrencies
	)
	const currencyList =
		currencyData !== undefined
			? getOptionsArray(currencyData, "short_code")
			: []
	const handleChange = (value: number | string | null) => setInputValue(value)
	const isNumberKey = (value: string) => isFinite(Number(value))
	const inputNumberValidation: React.KeyboardEventHandler = e => {
		if (e.key === "." && (inputValue === "" || inputValue === null)) {
			e.preventDefault()
		}
		if (
			!isNumberKey(e.key) &&
			e.key !== "Backspace" &&
			e.key !== "Enter" &&
			e.key !== "Tab" &&
			e.key !== "." &&
			!e.getModifierState("Control")
		) {
			e.preventDefault()
		}
	}

	// useEffect(() => {
	// 	api
	// 		.fetchCurrencies()
	// 		.then(rsp => {
	// 			console.log(rsp[0])
	// 			return getOptionsArray(rsp, "short_code")
	// 		})
	// 		.then(rsp => setCurrencyList(rsp))
	// }, [])
	console.log(inputValue)
	return (
		<Flex justify="center" className="container">
			<Space className="content-wrapper">
				<Select
					listHeight={132}
					showSearch
					className="currency-select"
					placeholder="pick"
					options={currencyList}
				/>
				<InputNumber
					type="number"
					value={inputValue}
					onChange={handleChange}
					onKeyDown={inputNumberValidation}
					controls={false}
				/>

				<Text>to</Text>
				<Select
					style={{ width: 75 }}
					placeholder="pick"
					options={[
						{ value: "USD", label: "USD" },
						{ value: "UAH", label: "UAH" },
						{ value: "EUR", label: "EUR" },
					]}
				/>
				<Button>convert</Button>
			</Space>
		</Flex>
	)
}

export default App

{
	/** explicit type="number" declaration required due to it not being set by the library */
}
{
	/* <input
				type="number"
				value={testInputvalue}
				onChange={e => {
					setTestInputvalue(e.target.value)
				}}
				onKeyDown={e => {
					if (e.key === "." && testInputvalue === "") {
						e.preventDefault()
					}
					if (
						!isNumberKey(e.key) &&
						e.key !== "Backspace" &&
						e.key !== "." &&
						!e.getModifierState("Control")
					) {
						e.preventDefault()
					}
				}}
			/>  remove when done */
}
